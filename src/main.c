/*
 * main.c
 *
 *  Created on: Sep 4, 2019
 *      Author: sinisav
 */

#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include "read_BMP_header.h"

int main(void)
{
	FILE* bmpPointer = fopen("C:\\Users\\sinisav\\Desktop\\steganography\\res\\MARBLES.BMP", "r");
	BITMAPFILEHEADER header;
	fread(&header, sizeof(BITMAPFILEHEADER), 1, bmpPointer);
	DIBHEADER imageInfo;
	fread(&imageInfo, sizeof(DIBHEADER), 1, bmpPointer);

	int16_t temp = bitmapType(&header);
	int8_t* charPoiner = (int8_t*)&temp;
	printf("%s%c", printMessages[0], *charPoiner++);
	printf("%c\n", *charPoiner);
	printf("%s%"PRId32"\n", printMessages[1], bitmapSize(&header));
	printf("%s%"PRId32"\n", printMessages[2], bitmapFileOffset(&header));
	for(int32_t counter = 0; counter < 11; counter++)
	{
		printf("%s%"PRId32"\n", printMessages[counter+3],funcPointersDIBHeader[counter](&imageInfo));
	}

	fclose(bmpPointer);
	return 0;
}
