void write_bit(uint8_t* rgbBytes, int8_t* message, int_least32_t message_length);
uint8_t get_bit(int8_t* message, int_least32_t bit_index);
void read_bit(uint8_t* rgbBytes, int_least32_t message_length);
