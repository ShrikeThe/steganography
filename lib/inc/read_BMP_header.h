/*
 * read_BMP_header.h
 *
 *  Created on: Sep 4, 2019
 *      Author: Sinisa Vidovic
 */

/*! \file read_BMP_header.h
 *  Header file that contains all items required for
 *  reading the information inside Bitmap file headers.
 */
#ifndef READ_BMP_HEADER_H_
#define READ_BMP_HEADER_H_

/*! \struct BITMAPFILEHEADER
 *  \brief Bitmap header structure.
 *
 *  Structure contains Bitmap Header fields that describe an image file.
 *  This header is mandatory for all bitmap images and is always 14 bytes in size.
*/
typedef struct __attribute__((packed)) tagBITMAPFILEHEADER {
	int16_t bitmapFileType; 		/*!< Signature of BMP: must be 4D42 hex. */
	int32_t bitmapFileSize; 		/*!< Size of BMP: in bytes. */
	int16_t bitmapFileReserved1;	/*!< Depends on application that creates the file. */
	int16_t bitmapFileReserved2;	/*!< Depends on application that creates the file. */
	int32_t bitmapFileOffset;		/*!< Offset to the start of image data in bytes.
	 	 	 	 	 	 	 	 	 * This is an important field because we can
	 	 	 	 	 	 	 	 	 * deduce the type of DIB header based on its
	 	 	 	 	 	 	 	 	 * size and therefore understand the functionality
	 	 	 	 	 	 	 	 	 * packed inside of our image file. */
} BITMAPFILEHEADER;

/*! \struct DIBHEADER
 *  \brief DIB header structure.
 *
 *  Structure contains DIB fields that further describe image.
 */
typedef struct __attribute__((packed)) tagDIBHEADER {
	int32_t DIBHeaderSize;			/*!< Size of DIB header structure. Must be 40. */
	int32_t bitmapWidth;			/*!< Image width in pixels. */
	int32_t bitmapHeight;			/*!< Image height in pixels. */
	int16_t colorPlanes;			/*!< Number of planes in image. Must be 1. */
	int16_t colorBitCount;			/*!< Number of bits per pixel. */
	int32_t compressionType;		/*!< Compression type. */
	int32_t imageSize;				/*!< Size of image data in bytes (including padding). */
	int32_t XPixelsPerMeter;		/*!< Horizontal resolution in pixels per meter. */
	int32_t YPixelsPerMeter;		/*!< Vertical resolution in pixels per meter. */
	int32_t colorsUsed;				/*!< Number of colors in image, or zero. */
	int32_t numberOfImportantColors;/*!< Number of important colors, or zero. */
} DIBHEADER;

/*! \brief Messages for header fields output.
 *
 * Messages that will be concatenated to outputs of the functions
 * which read Bitmap header structure fields. There is one message
 * entry for every field/function available.
 */
char* printMessages[] =
{
		"Bitmap type: ",
		"Bitmap size: ",
		"Bitmap header size: ",
		"Bitmap info header size: ",
		"Image width: ",
		"Image height: ",
		"Number of color planes: ",
		"Number of bits per pixel: ",
		"Compression type: ",
		"Image size (with padding): ",
		"Pixels per meter (x): ",
		"Pixels per meter (y): ",
		"Colors used: ",
		"Important colors used: "
};

/*! \fn bitmapType(BITMAPFILEHEADER* header)
 *  \brief Function reads the bitmapFileType field from the BITMAPFILEHEADER structure.
 * 	\param header is a pointer to BITMAPHEADER structure.
 * 	\return 2 byte integer. Each byte represents an ASCII character.
 * 	It has to return 4D42 which is BM in ASCII.
 */
int16_t bitmapType(BITMAPFILEHEADER* header);

/*! \fn bitmapSize(BITMAPFILEHEADER* header)
 *  \brief Function reads the bitmapFileSize field from the BITMAPFILEHEADER structure.
 *  \param header is a pointer to BITMAPHEADER structure.
 *  \return 4 byte integer that represents .bmp image size in bytes.
 */
int32_t bitmapSize(BITMAPFILEHEADER* header);

/*! \fn bitmapFileOffset(BITMAPFILEHEADER* header)
 *  \brief Function reads the bitmapFileOffset field from the BITMAPFILEHEADER structure.
 *  \param header is a pointer to BITMAPHEADER structure.
 *  \return 4 byte integer that represents offset of data
 *  segment in .bmp file from the begining of the file.
 */
int32_t bitmapFileOffset(BITMAPFILEHEADER* header);

/*! \fn DIBHeaderSize(DIBHEADER* imageInfo)
 *  \brief Function reads the DIBHeaderSize field from the DIBHEADER structure.
 *  \param imageInfo is a pointer to DIBHEADER structure.
 *  \return 4 byte integer that represents size of the DIB header.
 */
int32_t DIBHeaderSize(DIBHEADER* imageInfo);

/*! \fn imageWidth(DIBHEADER* imageInfo)
 *  \brief Function reads the bitmapWidth field from the DIBHEADER structure.
 *  \param imageInfo is a pointer to DIBHEADER structure.
 *  \return 4 byte integer that represents image width in pixels.
 */
int32_t imageWidth(DIBHEADER* imageInfo);

/*! \fn imageHeight(DIBHEADER* imageInfo)
 *  \brief Function reads the bitmapHeight field from the DIBHEADER structure.
 *  \param imageInfo is a pointer to DIBHEADER structure.
 *  \return 4 byte integer that represents image height in pixels.
 */
int32_t imageHeight(DIBHEADER* imageInfo);

/*! \fn colorPlanes(DIBHEADER* imageInfo)
 *  \brief Function reads the colorPlanes field from the DIBHEADER structure.
 *  \param imageInfo is a pointer to DIBHEADER structure.
 *  \return 4 byte integer that represents number of planes inside image file.
 */
int32_t colorPlanes(DIBHEADER* imageInfo);

/*! \fn colorBitCount(DIBHEADER* imageInfo)
 *  \brief Function reads the colorBitCount field from the DIBHEADER structure.
 *  \param imageInfo is a pointer to DIBHEADER structure.
 *  \return 4 byte integer that represents number of bits per pixel.
 */
int32_t colorBitCount(DIBHEADER* imageInfo);

/*! \fn compressionType(DIBHEADER* imageInfo)
 *  \brief Function reads the compressionType field from the DIBHEADER structure.
 *  \param imageInfo is a pointer to DIBHEADER structure.
 *  \return 4 byte integer that represents compression type applied to image.
 */
int32_t compressionType(DIBHEADER* imageInfo);

/*! \fn imageSize(DIBHEADER* imageInfo)
 *  \brief Function reads the imageSize field from the DIBHEADER structure.
 *  \param imageInfo is a pointer to DIBHEADER structure.
 *  \return 4 byte integer that represents the size of image data \n
 *   		including the padding that may occur at the end of pixel row.
 */
int32_t imageSize(DIBHEADER* imageInfo);

/*! \fn xPixelsPerMeter(DIBHEADER* imageInfo)
 *  \brief Function reads the XPixelsPerMeter field from the DIBHEADER structure.
 *  \param imageInfo is a pointer to DIBHEADER structure.
 *  \return 4 byte integer that represents the number of pixels \n
 *  		that would be found in meter of horizontal dimension of image.
 */
int32_t xPixelsPerMeter(DIBHEADER* imageInfo);

/*! \fn yPixelsPerMeter(DIBHEADER* imageInfo)
 *  \brief Function reads the YPixelsPerMeter field from the DIBHEADER structure.
 *  \param imageInfo is a pointer to DIBHEADER structure.
 *  \return 4 byte integer that represents the number of pixels \n
 *  		that would be found in meter of vertical dimension of image.
 */
int32_t yPixelsPerMeter(DIBHEADER* imageInfo);

/*! \fn colorsUsed(DIBHEADER* imageInfo)
 *  \brief Function reads the colorsUsed field from the DIBHEADER structure.
 *  \param imageInfo is a pointer to DIBHEADER structure.
 *  \return 4 byte integer that represents the number of colors \n
 *  		used in the .bmp image.
 */
int32_t colorsUsed(DIBHEADER* imageInfo);

/*! \fn numImportantColors(DIBHEADER* imageInfo)
 *  \brief Function reads the numOfImportantColors field from the DIBHEADER structure.
 *  \param imageInfo is a pointer to DIBHEADER structure.
 *  \return 4 byte integer that represents the number of important \n
 *  		colors used in the .bmp image.
 */
int32_t numImportantColors(DIBHEADER* imageInfo);

/*!
 *  \brief Array of function pointers for reading DIB header structure.
 *
 *  This array of function pointers allows us to read the entirety of DIB header structure \n
 *  by utilizing the "for loop" rather than having to call each function one at the time.
 */
extern int32_t (*funcPointersDIBHeader[]) (DIBHEADER*);

#endif /* READ_BMP_HEADER_H_ */
