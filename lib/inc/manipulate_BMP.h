/*! This is a doxygen comment */
char* read_text_data(char *fileName, int bytes);
unsigned char* read_BMP_data(char *fileName, int bytes);
int number_of_bytes_BMP(char *fileName);
int number_of_bytes_TXT(char *fileName);
