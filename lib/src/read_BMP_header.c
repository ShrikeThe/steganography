#include <stdint.h>
#include <inttypes.h>
#include "read_BMP_header.h"

extern char* printMessages[];

int32_t (*funcPointersDIBHeader[]) (DIBHEADER*) =
{
		DIBHeaderSize, imageWidth, imageHeight,
		colorPlanes,colorBitCount, compressionType,
		imageSize, xPixelsPerMeter, yPixelsPerMeter,
		colorsUsed, numImportantColors
};

int16_t bitmapType(BITMAPFILEHEADER* header)
{
	int16_t bitmapTypeChar = 0;
	int16_t temp = 0;
	int8_t* headerReader = (int8_t*)(&header->bitmapFileType);
	bitmapTypeChar = *headerReader++;
	temp = *headerReader;
	temp = temp << 8;
	bitmapTypeChar = bitmapTypeChar + temp;
	return bitmapTypeChar;
}

int32_t bitmapSize(BITMAPFILEHEADER* header)
{
	int32_t bitmapSize = header->bitmapFileSize;
	return bitmapSize;
}

int32_t bitmapFileOffset(BITMAPFILEHEADER* header)
{
	int32_t bitmapFileOffset = header->bitmapFileOffset;
	return bitmapFileOffset;
}

int32_t DIBHeaderSize(DIBHEADER* imageInfo)
{
	int32_t DIBHeaderSize = imageInfo->DIBHeaderSize;
	return DIBHeaderSize;
}

int32_t imageWidth(DIBHEADER* imageInfo)
{
	int32_t imageWidth = imageInfo->bitmapWidth;
	return imageWidth;
}

int32_t imageHeight(DIBHEADER* imageInfo)
{
	int32_t imageHeight = imageInfo->bitmapHeight;
	return imageHeight;
}

int32_t colorPlanes(DIBHEADER* imageInfo)
{
	int32_t colorPlanes = imageInfo->colorPlanes;
	return colorPlanes;
}

int32_t colorBitCount(DIBHEADER* imageInfo)
{
	int32_t colorBitCount = imageInfo->colorBitCount;
	return colorBitCount;
}

int32_t compressionType(DIBHEADER* imageInfo)
{
	int32_t compressionType = imageInfo->compressionType;
	return compressionType;
}

int32_t imageSize(DIBHEADER* imageInfo)
{
	int32_t imageSize = imageInfo->imageSize;
	return imageSize;
}

int32_t xPixelsPerMeter(DIBHEADER* imageInfo)
{
	int32_t xPixelsPerMeter = imageInfo->XPixelsPerMeter;
	return xPixelsPerMeter;
}

int32_t yPixelsPerMeter(DIBHEADER* imageInfo)
{
	int32_t yPixelsPerMeter = imageInfo->YPixelsPerMeter;
	return yPixelsPerMeter;
}

int32_t colorsUsed(DIBHEADER* imageInfo)
{
	int32_t colorsUsed = imageInfo->colorsUsed;
	return colorsUsed;
}

int32_t numImportantColors(DIBHEADER* imageInfo)
{
	int32_t importantColors = imageInfo->numberOfImportantColors;
	return importantColors;
}
