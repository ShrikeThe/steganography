#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <limits.h>
#include <time.h>
#include <string.h>
#include "steganography.h"
#define CHAR_OFFSET(b) ((b)/CHAR_BIT)
#define BIT_OFFSET(b) ((b)%CHAR_BIT)

int main()
{
    char poruka[] = "Hajde poruQa";
    srand(time(NULL));
    unsigned char pixeli[150];
    for (int i = 0; i < 150; i++){
        pixeli[i] = rand() % 255;
    }
    unsigned char reference[150];
    for (int i = 0; i<150;i++){
        reference[i] = pixeli[i];
    }
    for(int i = 0; i < 150;i++){
        if(!(i%8) && i!=0)
            printf("\n");
        printf("%4u",pixeli[i]);
    }
    write_bit(pixeli,poruka,sizeof(poruka));
    unsigned char differ[150];
    for(int i = 0; i < 150; i++){
        if((reference[i] == pixeli[i]) && (reference[i]%2==0))
            differ[i] = 0;
        if((reference[i] == pixeli[i]) && (reference[i]%2!=0))
            differ[i] = 1;
        if(reference[i] > pixeli[i])
            differ[i] = 0;
        if(reference[i] < pixeli[i])
            differ[i] = 1;      
    }
    printf("\nNovi pixeli:\n");
    for(int i = 0; i < 150;i++){
        if (!(i%8) && i !=0)
            printf("\n");
        printf("%4u",pixeli[i]);
    }
    printf("\nEnkodovano:\n");
    for(int i = 0; i < 150;i++){
        if (!(i%8) && i !=0)
            printf("\n");
        printf("%4u",differ[i]);
    }

    read_bit(pixeli,sizeof(poruka));
    return 0;
}

/* Function that takes bit from text message and writes it to LSB in RGB byte */ 
void write_bit(uint8_t* rgbBytes, int8_t* message, int_least32_t message_length)
{
    for (int_least32_t i = 0; i < message_length*CHAR_BIT; i++){
	/* Case when byte ends with 1. */
        if ( rgbBytes[i]%2 ){
            rgbBytes[i] &= (0xFE | get_bit(message,i));
        }
	/* Case when byte ends with 0. */
        else{
            rgbBytes[i] |= (0x00 | get_bit(message,i));
        }
    }
}

/* Function that reads LSBs in RGB bytes and assembles them into a text message. */
void read_bit(uint8_t* rgbBytes, int_least32_t message_length)
{
    int8_t message[message_length];
    memset(message,0,message_length);
    for(int_least32_t i = 0; i < message_length*CHAR_BIT; i++){

        message[i/CHAR_BIT] |= (get_bit((int8_t*)rgbBytes+i, 0) << (i%8));
    }
    printf("\nVasa poruka:\n");
    printf("%s",message);
}

/* Function that reads a particular bit from a text message. */
uint8_t get_bit(int8_t* message, int_least32_t bit_index)
{
    uint8_t bit = message[CHAR_OFFSET(bit_index)] & (1<<BIT_OFFSET(bit_index));
    return (bit != 0);
}
