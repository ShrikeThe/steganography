#include <stdio.h>
#include <string.h>
#include <errno.h>
#include "manipulate_BMP.h"
#include <stdlib.h>

int main()
{
    char txt_file_name[] = "hamlet.txt";
    char BMP_file_name[] = "MARBLES.BMP";
    int numBytesTXT = number_of_bytes_TXT(txt_file_name);
    int numBytesBMP = number_of_bytes_BMP(BMP_file_name);
    printf("Text: %d, BMP: %d",numBytesTXT,numBytesBMP); 
    char new_BMP_fileName[] = "encoded.BMP";      
    return 0;
}

char* read_text_data(char *fileName, int bytes)
{
    FILE *readFile;
    if((readFile = fopen(fileName,"r")) == NULL){
        printf("Error: %s\n",strerror(errno));
        exit(0);
    }
    char *poruka = (char*)malloc(bytes); 
    fread(poruka,1,bytes,readFile);
    return poruka;
}

unsigned char* read_BMP_data(char *fileName, int bytes)
{
    FILE *readFile;
    if((readFile = fopen(fileName,"r")) == NULL){
        printf("Error: %s\n",strerror(errno));
        exit(0);
    }
    unsigned char *BMP_data = (unsigned char*)malloc(bytes);
    fseek(readFile, 54, SEEK_SET); //jump over BMP header
    fread(BMP_data, 1, bytes, readFile);
    return BMP_data;
}

int number_of_bytes_TXT(char *fileName)
{
    FILE *readFile;
    if((readFile = fopen(fileName,"r")) == NULL){
        printf("Error: %s\n",strerror(errno));
        exit(0);
    }
    int bytes;
    for(bytes = 0; getc(readFile)!=EOF;bytes++);
    return bytes;
}

int number_of_bytes_BMP(char *fileName)
{
    FILE *readFile;
    if((readFile = fopen(fileName,"r")) == NULL){
        printf("Error: %s\n",strerror(errno));
        exit(0);
    }
    int bytes;
    for(bytes = 0; getc(readFile)!=EOF;bytes++);
    return bytes-54;
}
